﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class CameraMovement : MonoBehaviour
{
	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//
	//									Registered Touch Class									   //
	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//

	public class RegisteredTouch
	{
		public int id;
		public Touch touch;

		public RegisteredTouch(int id, Touch touch)
		{
			this.id = id;
			this.touch = touch;
		}
	}

	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//


	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//
	//									Pan Bounds Class										   //
	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//

	[Serializable]
	public class CPanBounds
	{
		public float UpDownMax;// = 10;
		public float LeftRightMax;// = 10;

		public Vector2 FlexiBounds;// {get; set;}
		float BasePanSpeed = 5f;
		float Speed = 0.2f;


		public Transform camTrans;

		public Vector3 PanOffset;
		public Vector2 BoundsOffset;

		//Constructor that takes camera transform
		public void InitBounds(Transform cam, CZoomBounds zoomBounds)
		{
			Debug.Log(cam == null);
			Debug.Log(zoomBounds == null);
			camTrans = cam;
			FlexiBounds = new Vector2(UpDownMax,LeftRightMax);
			//zoomBounds.IncreaseBoundsRelativeToZoom(this);
		}

		public float ControlSpeedByZoomFactor(CZoomBounds zoomBounds)
		{
			return BasePanSpeed + zoomBounds.ZoomPercentage * Speed;
		}

		//Calculates the pan offset with respect to the bounds(x,y) offsets
		public void CalculatePanOffset()
		{
			PanOffset = (-camTrans.right * BoundsOffset.x) + (-camTrans.up * BoundsOffset.y);
		}

//		public Vector3 GetCamPositionWithPanOffset()
//		{
//			return camTrans.position
//		}

		//Bounds check for panning.
		//Prevents camera from exceeding bounds by repositioning transform
		//when it's bounds(x,y) are greater than the absolute value of directional bounds
		public void CheckWithinBounds(Vector3 zoomPosition)
		{
			if(Mathf.Abs(BoundsOffset.x) > FlexiBounds.x)
			{
				if(BoundsOffset.x < 0)
				{
					BoundsOffset.x = -FlexiBounds.x;
				}
				else
				{
					BoundsOffset.x = FlexiBounds.x;
				}
			}

			if(Mathf.Abs(BoundsOffset.y) > FlexiBounds.y)
			{
				if(BoundsOffset.y < 0)
				{
					BoundsOffset.y = -FlexiBounds.y;
				}
				else
				{
					BoundsOffset.y = FlexiBounds.y;
				}
			}

			//Re-calculates PanOffset after we've figured out whether we are in/outside the bounds.
			CalculatePanOffset();

			//Setting camera position with respect to the zoom and pan offsets
			camTrans.position = zoomPosition + PanOffset;

//			Debug.Log("Zooming - Camera Position: "+camTrans.position);
//			Debug.Log("Zooming - Flexibounds: "+FlexiBounds);

		}
	}

	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//
	//									Zoom Bounds Class										   //
	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//

	[Serializable]
	public class CZoomBounds
	{
		public float MaxDepth;// = 470;
		public float MinDepth;// = 100;
	
		public float ZoomFactorX = 40f;
		public float ZoomFactorY = 40f;

		public Vector3 BaseCamPos;
		public Vector3 zoomPosition;

		[Range(0,1)]
		public float ZoomPercentage;
		[Range(0,1)]
		public float CameraBegin = 0.1f;

		public float ZoomTime = 4f;

		[Header("Camera's Position Live for testing!!")]
		public Vector3 CameraPosition;

		Vector3 backgroundPos;
		Transform cameraTrans;
		CameraMovement camera;

		public AnimationCurve ZoomCurve;

		Vector3 originalPos;
		Vector3 newPos;

		//Sets Default position of camera in relation to background position
		//References the Camera Transform
		//Sets default position of the camera
		//Sets default zoom position == to the default position


		public void SetDefaultPosition(/*Vector3 _backgroundPos,*/ GameObject _camera)
		{
			backgroundPos = new Vector3(0,0,0);
			camera = _camera.GetComponent<CameraMovement>();
			cameraTrans = _camera.transform;
			CameraPosition = cameraTrans.position;

			Vector3 defaultOffset = new Vector3();

			BaseCamPos = CalculateBounds(MaxDepth, defaultOffset);

			SetCamToMaxPos(defaultOffset);

		}

		public void GetTouchZoomNewPosition(Vector2 v1, Vector2 v2)
		{
			Vector2 middlePos = v1 + ((v2 - v1)/2);

			newPos = Camera.main.ScreenToWorldPoint (new Vector3(middlePos.x, middlePos.y, 0));

			Debug.Log("Touch - New Pos: "+newPos);
		}

		public void GetScrollNewPos(Vector3 mousePoint)
		{
			//newPos = Camera.main.ScreenToWorldPoint (new Vector3(mousePoint.x, mousePoint.y, 0));

			originalPos = cameraTrans.position;

			Debug.Log("Scroll - New Pos: "+newPos+" OriginalPos: "+originalPos);
		
		}



//		public void RegisterPinchZoom(Vector3 _newPos)
//		{
//			newPos = _newPos;
//			originalPos
//		}

		public void UpdatePinchZoom()
		{

		}


		public void IncreaseBoundsRelativeToZoom(CPanBounds panBounds)
		{
			panBounds.FlexiBounds = new Vector2(panBounds.LeftRightMax + (ZoomPercentage * ZoomFactorX), panBounds.UpDownMax + (ZoomPercentage * ZoomFactorY));

			//Debug.Log(panBounds.FlexiBounds);
		}

		public void GetZoomFactor()
		{
			float distance = Vector3.Distance(zoomPosition, CalculateBounds(MaxDepth, new Vector3()));

			if(distance > 0)
				CalculateZoomPercentage(distance);
			else
				ZoomPercentage = 0f;
		}

		public void CalculateZoomPercentage(float distance)
		{
			float range = MaxDepth - MinDepth;
			ZoomPercentage =  distance/range;
		}

		public float GetStartingPos(float startingBuffer)
		{
			float range = MaxDepth - MinDepth;

			float buffer = startingBuffer * range;

			Debug.Log("Buffer: "+buffer);

			return buffer;
		}

		public IEnumerator ZoomCameraCoroutine()
		{
			Debug.Log("Starting coroutine");

			float buffer = GetStartingPos(CameraBegin);
			float timer = 0f;
			float difference;
			float percentage;

			while (true)
			{
				timer += Time.deltaTime;

				percentage = timer/ZoomTime;
				if(timer >= ZoomTime)
				{
					percentage = 1;
				}

				float zoomPercentage = ZoomCurve.Evaluate(percentage); // value between 0 and 1
				//difference =  ZoomCurve.Evaluate(percentage);
				difference = buffer * zoomPercentage;
				zoomPosition = backgroundPos + (-cameraTrans.forward * (MaxDepth-difference));
				cameraTrans.position = zoomPosition;

				CalculateZoomPercentage(difference);

				Debug.Log("ZoomCamera - zoomPosition: "+zoomPosition);

				if(percentage == 1)
				{
					camera.runningZoomEntry = false;
					break;
				}


				yield return null;
			}



		}

		//Sets Cam to Max position with respect to the Max depth
		public void SetCamToMaxPos(Vector3 offset = new Vector3())
		{
			zoomPosition = backgroundPos + (-cameraTrans.forward * MaxDepth);
			cameraTrans.position = CalculateBounds(MaxDepth, offset);
		}

		//Sets Cam to Max position with respect to the Min depth
		public void SetCamToMinPos(Vector3 offset = new Vector3())
		{
			zoomPosition = backgroundPos + (-cameraTrans.forward * MinDepth);
			cameraTrans.position = CalculateBounds(MinDepth, offset);
		}

		//Returns the position of the camera from the background, with the required
		//depth
		public Vector3 CalculateBounds(float depth, Vector3 offset)
		{
			//zoomPosition = backgroundPos + (-cameraTrans.forward * depth);
			return (backgroundPos + (-cameraTrans.forward * depth)) + offset;
		}

		public Vector3 GetVectorPositionFromFloat(float pos)
		{
			return backgroundPos + (-cameraTrans.forward * pos);
		}

		//Returns the distance from a position to the background
		public float GetDistanceFromBase()
		{
			return Vector3.Distance(zoomPosition,backgroundPos);
		}

		//Bounds check for zoom.
		//Checks for the distance between the current cam position
		//and the max and min depths the camera can move.
		//If those limits haven't been reached, we default to the zoom
		//position with respect to the pan offset.
		public void CheckWithinBounds(Vector3 offset)
		{
			float length = GetDistanceFromBase();

			if(length > MaxDepth)
			{
				SetCamToMaxPos(offset);
			}
			else if(length < MinDepth)
			{
				SetCamToMinPos(offset);
			}
			else{
				cameraTrans.position = zoomPosition + offset;
			}

			GetZoomFactor();
		}

	}

	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//

	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//
	//									Camera Location											   //
	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//

	public class CameraLocation
	{
		public void MoveToLocation(float startingPos)
		{

		}
	}

	//---------------------------------------------------------------------------------------------//
	//---------------------------------------------------------------------------------------------//


	public CZoomBounds ZoomBounds;
	public CPanBounds PanBounds;

	public float MouseScrollSpeed = 50;

	Camera _camera;
	float velocity;


	//These used for scrolling all directions on pc using the arrow keys.
	//May be necessary to remove them for final build
	float HorizontalSpeed = 2f;
	float VerticalSpeed = 2f;

	float DefaultMouseWheelRotation;

	Vector2 currentInteractionPos;
	private bool panning = false;
	private bool hiding = false;
	bool runningZoomEntry = false;


	public bool activeInScreen = true;
	Rect screenRect = new Rect(0,0, Screen.width, Screen.height);

	List<RegisteredTouch> registeredTouches = new List<RegisteredTouch>();

	public void RegisterTouch(int id, Touch touch)
	{
		RegisteredTouch regT = new RegisteredTouch(id,touch);

		registeredTouches.Add(regT);

		//Debug.LogFormat("Touch Registered id: ({1})! Count of registeredTouches: ({0}) ",registeredTouches.Count, regT.id);
	}

	public void UnregisterTouch(int id)
	{
		List<RegisteredTouch> temp = new List<RegisteredTouch>();

		foreach(RegisteredTouch regT in registeredTouches)
		{
			if(id != regT.id)
				temp.Add(regT);
		}

		registeredTouches.Clear();

		foreach(RegisteredTouch regT in temp)
		{
			registeredTouches.Add(regT);
		}

		//Debug.LogFormat("Touch Unregistered! Count of registeredTouches: ({0}) ",registeredTouches.Count);

		if(registeredTouches.Count <= 1)
			InteractionStopped();
	}

	public void OnLoadBackground()
	{
		ZoomBounds.SetDefaultPosition(this.gameObject);
		//StartCoroutine(ZoomBounds.ZoomCameraCoroutine());
		PanBounds.InitBounds(_camera.transform, ZoomBounds);

	}

	public void Awake()
	{
		_camera = Camera.main;
		DefaultMouseWheelRotation = Input.GetAxis("Mouse ScrollWheel");

		//GameObject scene = GameObject.FindGameObjectWithTag("Scene");
		ZoomBounds.SetDefaultPosition(this.gameObject);
		PanBounds.InitBounds(_camera.transform, ZoomBounds);
		//PanBounds = new CPanBounds(_camera.transform);

//		if(scene != null)
//		{
			
			//StartCoroutine(ZoomBounds.ZoomCameraCoroutine());
//			
//		}
//		else
//		{
//			Debug.LogWarning("Base camera reference point not found! Consider tagging with \"Scene\"");
//		}


	}

	public void Start()
	{
		
	}

	public void Update()
	{
		if(runningZoomEntry)
		{}
		else
		{
			
		#if UNITY_IPHONE // For iPhone
				
			TouchesCheck();

			if(registeredTouches.Count == 1)
			{
				//Do panning logic
				Panning();
				ShowHideSandbox();
			}
			else if(registeredTouches.Count >= 2 && registeredTouches.Count < 4)
			{
				//Do PinchZoom stuff
				PinchZoom();
				ShowHideSandbox();
			}
			else if(registeredTouches.Count == 4)
			{
				hiding = true;
			}
		
		#else  // For PC

			if(Input.GetMouseButton(0))
			{
				Panning();
			}

			if(Input.GetMouseButtonUp(0))
				InteractionStopped();

			if(DefaultMouseWheelRotation != Input.GetAxis("Mouse ScrollWheel"))
				PinchZoom();

			if(Input.GetKey(KeyCode.LeftArrow))
				MoveLeftRight(HorizontalSpeed);
			else if(Input.GetKey(KeyCode.RightArrow))
				MoveLeftRight(-HorizontalSpeed);
			
			if(Input.GetKey(KeyCode.UpArrow))
				MoveUpDown(-VerticalSpeed);
			else if(Input.GetKey(KeyCode.DownArrow))
				MoveUpDown(VerticalSpeed);
		#endif
		}

	}

	public void TouchesCheck()
	{
		Touch[] touches = Input.touches;

		foreach(Touch touch in touches)
		{
			switch(touch.phase)
			{
			case TouchPhase.Began : 

				RegisterTouch(touch.fingerId, touch);
				break;
			case TouchPhase.Canceled : 
			case TouchPhase.Ended : 

				UnregisterTouch(touch.fingerId);
				break;

			}
		}
	}

	//Handles all panning of the camera
	//To be called on Update when a single touch is registered (for iPhone)
	//or a Left Mouse Down is registered (for PC)
	public void Panning()
	{
		//If we haven't started panning
		//Set a new current Interaction Position
		//of where we first touched/clicked the screen
		if(!panning)
		{
			#if UNITY_IPHONE
			currentInteractionPos = registeredTouches[0].touch.position;
			#else
			currentInteractionPos = Input.mousePosition;
			#endif

			panning = true;
		}
		else
		{
			Vector2 newPos;

			#if UNITY_IPHONE
			newPos = registeredTouches[0].touch.position;
			#else
			newPos = Input.mousePosition;
			#endif

			float hor = HorizontalSpeed * Input.GetAxis("Mouse X");
			float vert = VerticalSpeed * Input.GetAxis("Mouse Y");

			Vector2 difference = new Vector2(hor,vert);//newPos - currentMousePos;

			currentInteractionPos = newPos;

			velocity = PanBounds.ControlSpeedByZoomFactor(ZoomBounds);

			//Moving Left or Right
			float mouseX = difference.x;
			if(Mathf.Abs(mouseX) > 0.1f)
			{
				float h = (mouseX * velocity) * Time.deltaTime;
				MoveLeftRight(h);
			}

			//Moving Up or Down
			float mouseY = difference.y;
			if(Mathf.Abs(mouseY) > 0.1f)
			{
				float v = (mouseY * velocity) * Time.deltaTime;
				MoveUpDown(v);
			}

			//Check whether we are within the bounds with the new position
			PanBounds.CheckWithinBounds(ZoomBounds.zoomPosition);
		}
	}

	//Handles the zoom of the camera
	//To be called in Update when we register two-finger interaction (for iPhone)
	//or register a scroll using the mouse wheel (for PC)
	public void PinchZoom()
	{
		Touch[] touches = Input.touches;

		#if UNITY_IPHONE 
		if(touches.Length >= 2)
		#endif
		{

			float deltaMagnitudeDiff;
			//Vector3 focusPoint;

			#if UNITY_IPHONE 

				Vector2 touchZeroPrevPos = touches[0].position - touches[0].deltaPosition;
				Vector2 touchOnePrevPos = touches[1].position - touches[1].deltaPosition;
				
				ZoomBounds.GetTouchZoomNewPosition(touchZeroPrevPos, touchOnePrevPos);

				Debug.LogFormat("Touch One Pos: ({0}), Touch Two Pos: ({1})", touchZeroPrevPos, touchOnePrevPos);

				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touches[0].position - touches[1].position).magnitude;

				deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;


				
			#else

				float scroll = Input.GetAxis("Mouse ScrollWheel");
				deltaMagnitudeDiff = -scroll * MouseScrollSpeed;
				DefaultMouseWheelRotation = scroll;

				ZoomBounds.GetScrollNewPos(Input.mousePosition);
						
			#endif

			//Apply the amount of pinch/scroll to the zoomPosition
			//Then check whether we are still within the bounds

			//Debug.Log("Zooming - focusPoint: "+focusPoint);

			if (!screenRect.Contains(Input.mousePosition))
				return;

			ZoomBounds.zoomPosition -= _camera.transform.forward * deltaMagnitudeDiff;

			PanBounds.CheckWithinBounds(ZoomBounds.zoomPosition);

			ZoomBounds.CheckWithinBounds(PanBounds.PanOffset);
			
			ZoomBounds.IncreaseBoundsRelativeToZoom(PanBounds);



		}

	}

	public void MoveLeftRight(float velocity)
	{
		PanBounds.BoundsOffset.x += velocity;

		//PanBounds.BoundsOffset.x -= velocity;
	}

	public void MoveUpDown(float velocity)
	{
		PanBounds.BoundsOffset.y += velocity;

		//PanBounds.BoundsOffset.y -= velocity;
	}

	public void InteractionStopped()
	{
		panning = false;
	}
}
